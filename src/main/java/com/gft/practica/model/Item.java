package com.gft.practica.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Item {

    private String name;
    private String[] tags;
    private Double price;
    private Short quantity;



}
