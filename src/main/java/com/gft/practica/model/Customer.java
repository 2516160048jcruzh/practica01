package com.gft.practica.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Customer {

    private String gender;
    private Short age;
    private String email;
    private Short satisfaction;

}
