package com.gft.practica.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
public class Sale {

private String id;
private Date saleDate;
private List<Item>items;
private String storeLocation;
private Customer customer;
private Boolean couponUsed;
private String purchaseMethod;

}
