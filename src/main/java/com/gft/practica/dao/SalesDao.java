package com.gft.practica.dao;

import com.gft.practica.model.Sale;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface SalesDao extends ReactiveMongoRepository<Sale,String> {
}
