package com.gft.practica.service;

import org.springframework.stereotype.Service;

import com.gft.practica.model.Sale;

import reactor.core.publisher.Flux;

@Service
public interface ISaleService {

     Flux<Sale>getAllSales();

}
