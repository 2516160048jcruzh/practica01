package com.gft.practica.service;


import com.gft.practica.dao.SalesDao;
import com.gft.practica.model.Sale;

import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;

@Slf4j
public class SaleService implements ISaleService {
	
    @Autowired
    private SalesDao salesDao;

    @Override
    public Flux<Sale> getAllSales() {
        return salesDao.findAll().doOnNext(s->log.info(s.getId()));
    }
}
