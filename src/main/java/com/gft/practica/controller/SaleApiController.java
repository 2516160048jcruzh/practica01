package com.gft.practica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gft.practica.dao.SalesDao;
import com.gft.practica.model.Sale;
import com.gft.practica.service.ISaleService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sun.util.logging.resources.logging;

@RestController
@RequestMapping("/v1")

public class SaleApiController {

    @Autowired
    //private ISaleService saleService;
    private SalesDao dao;
    

//    @GetMapping("/sales")
//    public Mono<ResponseEntity<Sale>>getAllSales(){
//
//
//    	
//        return saleService.getAllSales()
//                .next()
//                .flatMap(sale -> Mono.just(ResponseEntity.ok(sale)));
//    }
    
    
    @GetMapping()
    public Flux<Sale> index(){
    	Flux<Sale> sales=dao.findAll();
    	return sales;
    	
    }

    @GetMapping("/prueba")
    public  ResponseEntity<String>prueba(){



        return  new ResponseEntity<>("Metodo de prueba", HttpStatus.OK);
    }

}
